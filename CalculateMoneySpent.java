import java.util.Scanner;

public class CalculateMoneySpent {
    public static void main(String[] args) {
        String name;
        int moneySpent, totalDays = 7, total = 0;
        double average;

        Scanner keyboard = new Scanner(System.in);

        System.out.println("=======================================================================================");
        System.out.println("## Program Java Menghitung Total pengeluaran setiap minggu dan rata-rata pengeluaran ##");
        System.out.println("=======================================================================================");
        System.out.println();

        System.out.print("Hey, what is your name ?" +"\n");
        name = keyboard.next();
        
        System.out.println("How much money did you spend at the club on Monday?");
        moneySpent = keyboard.nextInt();
        total += moneySpent;

        System.out.println("How much money did you spend at the club on Tuesday?");
        moneySpent = keyboard.nextInt();
        total += moneySpent;
        
        System.out.println("How much money did you spend at the club on Wednesday?");
        moneySpent = keyboard.nextInt();
        total += moneySpent;

        System.out.println("How much money did you spend at the club on Thursday?");
        moneySpent = keyboard.nextInt();
        total += moneySpent;

        System.out.println("How much money did you spend at the club on Friday?");
        moneySpent = keyboard.nextInt();
        total += moneySpent;

        System.out.println("How much money did you spend at the club on Saturday?");
        moneySpent = keyboard.nextInt();
        total += moneySpent;

        System.out.println("How much money did you spend at the club on Sunday?");
        moneySpent = keyboard.nextInt();
        total += moneySpent;

        average = (double) total/totalDays;

        System.out.println("The Calculation Results:");
        System.out.println("Hi " + name+"." +"\n"+"Your total expenditure at the club this week is $" + total + "\n" + "With an Average daily expenditure of $" + average);

        keyboard.close();
    }

}